/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ic.ecom.cdp.msb.actions;

import java.util.Collections;

import org.apache.unomi.api.Event;
import org.apache.unomi.api.Metadata;
import org.apache.unomi.api.Profile;
import org.apache.unomi.api.PropertyType;
import org.apache.unomi.api.actions.Action;
import org.apache.unomi.api.actions.ActionExecutor;
import org.apache.unomi.api.services.EventService;
import org.apache.unomi.api.services.ProfileService;

/**
 * Increments the number of times the user associated with the profile tweeted.
 */
public class IncrementMenFashionViewAction implements ActionExecutor {
 
    private static final String MEN_FASION_VIEW_COUNT_PROPERTY = "menFashionViewCount";
  
    
    private static final String TARGET = "profiles";

    private ProfileService service;

    public int execute(Action action, Event event) {
        final Profile profile = event.getProfile();
        Integer menFashionViewCount = (Integer) profile.getProperty(MEN_FASION_VIEW_COUNT_PROPERTY);
     

        if (menFashionViewCount == null ) {
            // create tweet number property type
            PropertyType propertyType = new PropertyType(new Metadata(event.getScope(), MEN_FASION_VIEW_COUNT_PROPERTY, MEN_FASION_VIEW_COUNT_PROPERTY, "Number of times a user viewed men fashion items"));
            propertyType.setValueTypeId("integer");
            propertyType.getMetadata().setTags(Collections.singleton("social"));
            propertyType.setTarget(TARGET);
            service.setPropertyType(propertyType);

            menFashionViewCount = 0;
            
        }

        profile.setProperty(MEN_FASION_VIEW_COUNT_PROPERTY, menFashionViewCount + 1);
             return EventService.PROFILE_UPDATED;
    }

    public void setProfileService(ProfileService service) {
        this.service = service;
    }

   
}
